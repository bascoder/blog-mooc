# Blog WebApp in Ruby on Rails #
This project is developed as part of the [https://www.coursera.org/course/webapplications](https://www.coursera.org/course/webapplications) from the University Of New Mexico on Coursera.
It's developed in Ruby on Rails and allows the admin to create blogs and visitors to post comments to a blog.